/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the abstract		*
* 	protocol, hold the share code		*
*****************************************/

#include "CommunicationProtocol.h"


/****************************************************************
* create the protocol, and save the socket						*
****************************************************************/
CommunicationProtocol::CommunicationProtocol(int socketNumber){
	this->socketNum = socketNumber;
}

/****************************************************************
* close the socket												*
****************************************************************/
CommunicationProtocol::~CommunicationProtocol(){
	close(this->socketNum);
}

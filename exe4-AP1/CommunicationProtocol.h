/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the abstract		*
* 	protocol, hold the share code		*
*****************************************/

#ifndef COMMUNICATIONPROTOCOL_H_
#define COMMUNICATIONPROTOCOL_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

/****************************************************************
* the abstract protocol											*
*****************************************************************/
class CommunicationProtocol {
	protected:
		int socketNum;
		/****************************************************************
		* create the protocol, and save the socket						*
		****************************************************************/
		CommunicationProtocol(int socketNumber);

	public:
		/****************************************************************
		* close the socket and delete the protocol						*
		*****************************************************************/
		virtual ~CommunicationProtocol();
		/****************************************************************
		* send the buffer to the socket									*
		*****************************************************************/
		virtual int sendToSokect(char*buffer,int sizeMsg)= 0;
		/****************************************************************
		* get from the socket to the buffer								*
		*****************************************************************/
		virtual int receiveFromSocket(char*buffer, int sizeBuf)= 0;
};

#endif /* COMMUNICATIONPROTOCOL_H_ */

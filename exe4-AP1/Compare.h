/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare logic of 	*
* 	Professional						*
****************************************/
#ifndef INCLUDE_COMPARE_H_
#define INCLUDE_COMPARE_H_
#include "Professional.h"
/****************************************************************
* Abstract class of the compare logic							*
*****************************************************************/
class Compare {
	public:

		/************************************************************
		 * return true if first is bigger then second,	otherwise 	*
		 * 	return false											*
		 ************************************************************/
		virtual bool compare(Professional* first, Professional *second) = 0;
		virtual ~Compare();
};

#endif /* INCLUDE_COMPARE_H_ */

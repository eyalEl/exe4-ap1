/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there age				*
****************************************/

#include "CompareAge.h"

/********************************************************************
 * check if the age of the first is bigger, return true otherwise	*
 * 	return false													*
 ********************************************************************/
bool CompareAge::compare(Professional* first, Professional *second){
	return first->getAge()>second->getAge();
}

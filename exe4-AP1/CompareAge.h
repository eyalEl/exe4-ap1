/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there age				*
****************************************/

#ifndef INCLUDE_COMPAREAGE_H_
#define INCLUDE_COMPAREAGE_H_
#include "Compare.h"
/****************************************************************
* compare professional according to there age's					*
*****************************************************************/
class CompareAge:public Compare {
public:

	/************************************************************
	 * return true if first is bigger then second,	otherwise 	*
	 * 	return false											*
	 ************************************************************/
	bool compare(Professional* first, Professional *second);
};

#endif /* INCLUDE_COMPAREAGE_H_ */

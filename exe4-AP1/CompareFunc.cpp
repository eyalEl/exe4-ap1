/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim*
* Exercise Name: exe5               	*
* File description: the object that 	*
* hold the logic of compare				*
****************************************/

#include "CompareFunc.h"
/********************************************************************
 * check compare between the two professional with the logic		*
 ********************************************************************/
bool CompareFunc::operator()(Professional* first, Professional * second){
	return this->logic->compare(first, second);
}

/********************************************************************
 * set the logic of the compare										*
 ********************************************************************/
void CompareFunc::setTypeCompare(Compare * func){
	delete this->logic;
	this->logic = func;
}

/************************************************************
 * delete the "this" and the logic							*
 ************************************************************/
void CompareFunc::deepDelete(){
	delete this->logic;
	delete this;
}

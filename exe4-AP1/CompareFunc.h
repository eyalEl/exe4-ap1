/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the object that 	*
* hold the logic of compare				*
****************************************/

#ifndef INCLUDE_COMPAREFUNC_H_
#define INCLUDE_COMPAREFUNC_H_
#include "Compare.h"
/****************************************************************
* holds the compare logic										*
*****************************************************************/
class CompareFunc {
	private:
		Compare* logic;
	public:
		/************************************************************
		 * return true if first is bigger then second, false other-	*
		 * 	wise, according the compare								*
		 ************************************************************/
		bool operator()(Professional* first, Professional * second);
		/************************************************************
		 * set the compare logic									*
		 ************************************************************/
		void setTypeCompare(Compare * func);
		/************************************************************
		 * delete the object and his members						*
		 ************************************************************/
		void deepDelete();

};

#endif /* INCLUDE_COMPAREFUNC_H_ */

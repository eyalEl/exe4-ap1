/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there id				*
****************************************/
#include "CompareId.h"

/********************************************************************
 * check if the id of the first is smallser, return true otherwise	*
 * 	return false													*
 ********************************************************************/
bool CompareId::compare(Professional* first, Professional *second){
	return (first->getId()<second->getId());
}

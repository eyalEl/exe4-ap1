/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there id				*
****************************************/
#ifndef INCLUDE_COMPAREID_H_
#define INCLUDE_COMPAREID_H_
#include "Compare.h"

/****************************************************************
* compare professional according to there id's					*
*****************************************************************/
class CompareId:public Compare {
	public:

		/************************************************************
		 * return true if first is smaller then second,	otherwise 	*
		 * 	return false											*
		 ************************************************************/
		bool compare(Professional* first, Professional *second);
};

#endif /* INCLUDE_COMPAREID_H_ */

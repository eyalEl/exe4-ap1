/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there number of movies	*
****************************************/
#include "CompareMovies.h"

/********************************************************************
 * check if the number of movies of the first is bigger, return true*
 *  otherwise return false											*
 ********************************************************************/
bool CompareMovies::compare(Professional* first, Professional *second){
	return first->getNumberOfMovies()>second->getNumberOfMovies();
}

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: compare two prof	*
* 	according to there number of movies	*
****************************************/
#ifndef INCLUDE_COMPAREMOVIES_H_
#define INCLUDE_COMPAREMOVIES_H_
#include "Compare.h"
/****************************************************************
* compare professional according to there movies				*
*****************************************************************/
class CompareMovies:public Compare {
	public:
		/************************************************************
	 	 * return true if first is bigger then second,	otherwise 	*
	 	 * 	return false											*
	 	 ************************************************************/
		bool compare(Professional* first, Professional *second);
};

#endif /* INCLUDE_COMPAREMOVIES_H_ */

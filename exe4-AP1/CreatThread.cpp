/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: Create the communication*
* with all users create a thread to each	*
* costumer			*
****************************************/
#include "CreatThread.h"

/********************************************************************
 * Constructor															  *
 * call the function creat tube to create a way to communicate with server*
 ********************************************************************/
CreatThread::CreatThread(string typeCommunicate, string portNumber) {
	creatTube(typeCommunicate,portNumber);

}

/********************************************************************
 *the function create a communication between the server and the world	*
 ********************************************************************/
void CreatThread::creatTube(string typeCommunicate, string portNumber){
	this->theSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (this->theSocket < 0) {
		perror("error creating socket");
	}
	memset(&this->sin, 0, sizeof(this->sin));
	this->sin.sin_family = AF_INET;
	this->sin.sin_addr.s_addr = INADDR_ANY;
	int port;
	istringstream (portNumber)>>port;
	this->sin.sin_port = htons(port);
	if (bind(this->theSocket,(struct sockaddr *) &this->sin,
			sizeof(this->sin)) < 0) {
		perror("error binding socket");
	}
	if (listen(this->theSocket, 5) < 0) {
		perror("error listening to a socket");
	}
}

/********************************************************************
 * Destruction														*
 ********************************************************************/
CreatThread::~CreatThread() {
}

/********************************************************************
 * every time when there is a new client we open a thread and a socket for him	*
 ********************************************************************/
void CreatThread::start(){
	while(true){
		pthread_t tStart;
	    struct sockaddr_in client_sin;
	    this->sin = client_sin;
	    unsigned int addr_len = sizeof(this->sin);
	    int* client_sock = new int;
	    *client_sock = accept(this->theSocket,(struct sockaddr *) &this->sin,  &addr_len);
	    if (*client_sock < 0) {
	        perror("error accepting client");
	    }
	    int status = pthread_create(&tStart,NULL, Menu::startRunning, (void*)client_sock);
	    if(status){
	    	perror("pthread creating went wrong!");
	    }
	}
}

/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: Create the communication*
* with all users create a thread to each	*
* costumer									*
*********************************************/


#ifndef CREATTHREAD_H_
#define CREATTHREAD_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "Menu.h"
#include <unistd.h>
#include <pthread.h>
#include <string.h>
using namespace std;

class CreatThread {
private:

	int theSocket;

protected:
	struct sockaddr_in sin;
public:

	/********************************************************************
	 * Constructor															  *
	 * call the function creat tube to create a way to communicate with server*
	 ********************************************************************/
	CreatThread(string typeCommunicate, string portNumber);

	/********************************************************************
	 *the function create a communication between the server and the world	*
	 ********************************************************************/
	void creatTube(string typeCommunicate, string portNumber);

	/********************************************************************
	 * every time when there is a new client we open a thread and a socket for him	*
	 ********************************************************************/
	void start();

	/********************************************************************
	 * Destruction														*
	 ********************************************************************/
	virtual ~CreatThread();
};


#endif /* CREATTHREAD_H_ */

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Director, type of 	*
* 	professional						*
****************************************/

#ifndef DIRECTOR_H_
#define DIRECTOR_H_
#include"Professional.h"
#include<string>
using namespace std;
/****************************************************************
* the class of the director										*
*****************************************************************/
class Director:public Professional {
	public:
		/********************************************************************
		 * create new director												*
		 ********************************************************************/
		Director(unsigned int id, string name, string description, int age, string gen);
		/********************************************************************
		* print the information that director need to print when the user	*
		* 	 ask for														*
		*********************************************************************/
		void print(InputOutput*io);
		/********************************************************************
		* delete the Director												*
		*********************************************************************/
		~Director();
};

#endif /* DIRECTOR_H_ */

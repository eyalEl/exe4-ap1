/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: get the input from 	*
* 	the user and send the output		*
****************************************/
#include "InputOutput.h"
#include "Tcp.h"
#include "ProfessionalFactory.h"

/****************************************************************
* create the tcp protocol, with the socket, create the buffer	*
* 	and clear it												*
*****************************************************************/
InputOutput::InputOutput(int socketNumber){
	this->buffer = new char[BUFFER_SIZE];
	this->clearBuffer();
	this->protocol = new Tcp(socketNumber);
}

/****************************************************************
* delete the buffer and the protocol							*
*****************************************************************/
InputOutput::~InputOutput(){
	delete this->protocol;
	delete this->buffer;
}

/****************************************************************
* read the all line of the order, take the order from the line	*
* 	and the rest of the line save in the buffer, and return the	*
* order 														*
*****************************************************************/
int InputOutput::getNextOrder(){
	this->clearBuffer();
	this->protocol->receiveFromSocket(this->buffer, BUFFER_SIZE);
	if(this->buffer[strlen(this->buffer)-1]== '\n'){
		this->buffer[strlen(this->buffer)-1] = '\0';
	}
	this->theBuffer = string(this->buffer); //convert to string
	size_t index = this->theBuffer.find(PARSER_BETWEEN_PARAM); //take the order from the line
	if(string::npos==index){ //if there is just an order in the line
		int order = atoi(this->theBuffer.c_str()); //take the order
		this->theBuffer = "";  //clear the buffer
		return order;
	}
	int order = atoi(this->theBuffer.substr(0,index).c_str()); //take the order
	this->theBuffer = this->theBuffer.substr(index+1);	//delete the order from the buffer
	return order;
}

/****************************************************************
* clear the buffer												*
*****************************************************************/
void InputOutput::clearBuffer(){
	char * temp = this->buffer;
	while (*temp){
		*temp = '\0';
		temp++;
	}
}

/****************************************************************
* enter the string to the buffer								*
*****************************************************************/
void InputOutput::enterStringToBuffer(string str){
	this->theBuffer +=str;
}
/****************************************************************
 * get next string from the user, find the next word, delete it *
 * from the buffer and return the word							*
 ****************************************************************/
string InputOutput::getNextString(){
	size_t index = this->theBuffer.find(PARSER_BETWEEN_PARAM); //find the next word
	if(string::npos ==index){	//if the word is the last one
		string temp = this->theBuffer;
		this->theBuffer = "";
		return temp;
	}
	string word = this->theBuffer.substr(0,index); //find the word
	this->theBuffer = this->theBuffer.substr(index+1);	//delete the word from the buffer
	return word;
}
/****************************************************************
 * get next integer number	,find the next number, delete it	*
 * from the buffer and return the number						*
 ****************************************************************/
int InputOutput::getNextInt(){
	size_t index = this->theBuffer.find(PARSER_BETWEEN_PARAM);
	if(string::npos ==index){ //if the number is the last word in the buffer
		int  temp = atoi(this->theBuffer.c_str());
		this->theBuffer = "";	//clear the buffer
		return temp;
	}
	int num = atoi(this->theBuffer.substr(0,index).c_str()); //take the number from the buffer
	this->theBuffer = this->theBuffer.substr(index+1); //delete the number from the number
	return num;
}
/****************************************************************
 * get next unsigned int number	,find the next number, delete it*
 * from the buffer and return the number						*
 ****************************************************************/
unsigned int InputOutput::getNextUnsignedInt(){
	size_t index = this->theBuffer.find(PARSER_BETWEEN_PARAM);
	if(string::npos ==index){ //if the number is the last word in the buffer
		unsigned int temp = atoi(this->theBuffer.c_str()); //take the number
		this->theBuffer = "";//clear the buffer
		return temp;
	}
	unsigned int num = atoi(this->theBuffer.substr(0,index).c_str()); //take the number from the buffer
	this->theBuffer = this->theBuffer.substr(index+1); //delete the number from the number
	return num;
}
/****************************************************************
 * get next double number ,find the next number, delete it		*
 * from the buffer and return the number						*						*
 ****************************************************************/
double InputOutput::getNextDouble(){
	size_t index = this->theBuffer.find(PARSER_BETWEEN_PARAM);
	if(string::npos ==index){ //if the number is the last word in the buffer
		double temp = atof(this->theBuffer.c_str()); //take the number
		this->theBuffer = ""; //clear the buffer
		return temp;
	}
	double num = atof(this->theBuffer.substr(0,index).c_str()); //take the number from the buffer
	this->theBuffer = this->theBuffer.substr(index+1); //delete the number from the number
	return num;
}
/***************************************************************
 * get next string up to the end of the line			*
 ****************************************************************/
string 	InputOutput::getUpToEndLine(){
	string temp = this->theBuffer;
	this->theBuffer= "";
	return temp;
}
/****************************************************************
 * print the string in the input				*
 ****************************************************************/
void InputOutput::print(string toPrint){
	this->enterStringToBuffer(toPrint);
}

/****************************************************************
 * print the int number in the input				*
 ****************************************************************/
void InputOutput::print(int toPrint){
	ostringstream os;
	os<<toPrint;
	this->enterStringToBuffer(os.str());
}
/****************************************************************
 * print the double number in the input				*
 ****************************************************************/
void InputOutput::print(double toPrint){
	ostringstream os;
	os<<toPrint;
	this->enterStringToBuffer(os.str());
}

/****************************************************************
 * take the all buffer that was up to now and send it to to the *
 * 	socket														*
 ****************************************************************/
void InputOutput::print(){
	this->clearBuffer();
	if(this->theBuffer.compare("")==0){
		this->theBuffer= "#";
	}
	strcpy(this->buffer, this->theBuffer.c_str());
	this->protocol->sendToSokect(this->buffer,this->theBuffer.size());
	this->theBuffer = "";
	this->clearBuffer();
}

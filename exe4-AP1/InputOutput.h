/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: get the input from 	*
* 	the user and send the output		*
****************************************/
#ifndef DEBUG_INPUTOUTPUT_H_
#define DEBUG_INPUTOUTPUT_H_
#include<string>
#include<list>
#include<iostream>
#include<cstdlib>
#include<sstream>
#include"Movie.h"
#include"CommunicationProtocol.h"

#define BUFFER_SIZE 4096
#define PARSER_BETWEEN_PARAM ' '

/****************************************************************
* the input output class, send the needed string to the needed	*
* 	place and read from there the input for the program			*
*****************************************************************/
class InputOutput {
	private:
		char* buffer;
		string theBuffer;
		CommunicationProtocol* protocol;
		/****************************************************************
		* clear the all buffer for the next read						*
		*****************************************************************/
		void clearBuffer();
		/****************************************************************
		* copy the string to the buffer									*
		*****************************************************************/
		void enterStringToBuffer(string str);
	public:
		/****************************************************************
		* create the right protocol, and bind in the socket				*
		*****************************************************************/
		InputOutput(int socketNumber);
		/****************************************************************
		* close the socket and delete the protocol						 *
		*****************************************************************/
		~InputOutput();
		/****************************************************************
		* get the next order to do										*
		*****************************************************************/
		int getNextOrder();
		/****************************************************************
		 * get next string 						*
		 ****************************************************************/
		string getNextString();
		/****************************************************************
		 * get next integer number					*
		 ****************************************************************/
		int getNextInt();
		/****************************************************************
		 * get next unsigned int number					*
		 ****************************************************************/
		unsigned int getNextUnsignedInt();
		/****************************************************************
		 * get next double number					*
		 ****************************************************************/
		double getNextDouble();
		/****************************************************************
		 * get next string up to the end of the line			*
		 ****************************************************************/
		string getUpToEndLine();
		/****************************************************************
		 * print the string in the input				*
		 ****************************************************************/
		void print(string toPrint);
		/****************************************************************
		 * print the int number in the input				*
		 ****************************************************************/
		void print(int toPrint);
		/****************************************************************
		 * print the double number in the input							*
		 ****************************************************************/
		void print(double toPrint);
		/****************************************************************
		* send what in the buffer to the socket							*
		*****************************************************************/
		void print();
};

#endif /* DEBUG_INPUTOUTPUT_H_ */

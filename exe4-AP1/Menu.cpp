/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the menu run the 	*
* 	orders form the user, get the input *
* and send it to the match function		*
****************************************/
#include"Menu.h"
#include"InputOutput.h"
#include<string>
#include<iostream>
#include<sstream>
using namespace std;
#define PARSER_BETWEEN_CODES ','
#define END_PROGRAM -1
enum Orders{ADD_MOVIE=1,ADD_PROF,ADD_PROF_TO_MOVIE,ADD_MOVIE_TO_GENRE,
	SET_SORT_MOVIE_PERSONAL,PRINT_MOVIES_PERSONAL,PRINT_MOVIE,
	CONNECT_MOVIE, PRINT_MOVIES_OF_PROF, DELETE_MOVIE,DELETE_PROF,
	DELETE_PROF_FROM_MOVIE,PRINT_ALL_MOVIES,PRINT_ALL_PROFS,
	PRINT_MOVIES_OF_GENRE};
//range of movie information
#define LEN_MOVIE_MIN 0
#define YEAR_MOVIE_MIN 0
#define YEAR_MVOIE_MAX 2015
#define RANK_MOVIE_MIN 0
#define RANK_MOVIE_MAX 10

//range of Professional information
#define AGE_PROF_MIN 0
#define AGE_PROF_MAX 120

#define MSG_FAIL "Failure"
#define MSG_SUCC "Success"


/********************************************************************
 * create the movie and professional collection and the factory of  *
 *	the professional												*
 ********************************************************************/
Menu::Menu(){
	this->movies = new MovieCollection();
	this->profFact = new ProfessionalFactory();
	this->professonals = new ProfessionalCollection();

}

/********************************************************************
 * delete the all collections										*
 ********************************************************************/
Menu::~Menu(){
	delete this->movies;
	delete this->profFact;
	delete this->professonals;
}

/********************************************************************
 * run the option of the user and send to the match function 		*
 * 	according to the user order										*
 ********************************************************************/
void Menu::start(int socketNumber){
	InputOutput* io;
	io = new InputOutput(socketNumber);
	int order;
	Result r;
	order = io->getNextOrder();//get first order
	while(END_PROGRAM != order){
		switch(order){
		case(ADD_MOVIE):
				r = this->newMovie(io);
				break;
		case(ADD_PROF):
				r = this->newprof(io);
				break;
		case(ADD_PROF_TO_MOVIE):
				r = this->addProfToMovie(io);
				break;
		case(ADD_MOVIE_TO_GENRE):
				r = this->addMovieToGenre(io);
				break;
		case(SET_SORT_MOVIE_PERSONAL):
				r = this->setSortProfForMovie(io);
				break;
		case(PRINT_MOVIES_PERSONAL):
				r = this->printProfOfMovie(io);
				break;
		case(PRINT_MOVIE):
				r = this->printMovie(io);
				break;
		case(CONNECT_MOVIE):
				r = this->connectMovie(io);
				break;
		case(PRINT_MOVIES_OF_PROF):
				r = this->printMoviesOfProf(io);
				break;
		case(DELETE_MOVIE):
				r = this->deleteMovie(io);
				break;
		case(DELETE_PROF):
				r = this->deleteProf(io);
				break;
		case(DELETE_PROF_FROM_MOVIE):
				r = this->deleteProfFromMovie(io);
				break;
		case(PRINT_ALL_MOVIES):
				this->movies->printAllMovies(io);
				r = SUCCSESS_NO_PRINT; //no need to print
				break;
		case(PRINT_ALL_PROFS):
				this->professonals->printAllProf(io);
				r = SUCCSESS_NO_PRINT; //no need to print
				break;
		case(PRINT_MOVIES_OF_GENRE):
				r = this->printMoviesOfGenre(io);
				break;
		default:
				break;
		}
		//if need to print something
		if(SUCCSESS == r){
			string output = MSG_SUCC;
			io->print(output);
			io->print("\n");
		} else if(FALIURE == r){
			string output = MSG_FAIL;
			io->print(output);
			io->print("\n");
		}
		io->print();
		order = io->getNextOrder();
	}
	delete io;
}

/********************************************************************
 * get the data of the movie that can be illegal, return true if	*
 * 	the all data is legal, false other wise							*
 ********************************************************************/
bool Menu::infoOfMovieIsLegal(string code, int len, int year, double rate){
	if(len<LEN_MOVIE_MIN){ //check the len of the movie in the right range
		return false;
	}
	if(year<YEAR_MOVIE_MIN||year>YEAR_MVOIE_MAX){ //check the year of the movie in the right range
		return false;
	}
	if(rate<RANK_MOVIE_MIN||rate>RANK_MOVIE_MAX){ //check the rate of the movie in the right range
		return false;
	}
	if(this->movies->isCodeExist(code)){ //check if the code of the movie is exist in the collection of the movies
		return false;
	}
	return true;
}

/********************************************************************
 * get the data of the prof that can be illegal, return true if		*
 * 	the all data is legal, false other wise							*
 ********************************************************************/
bool Menu::infoOfProfIsLegal(int tp ,unsigned int id, int age, string gendre){
	if(tp<PROF_TYPE_MIN||tp>PROF_TYPE_MAX){
			return NULL;
		}
	if(age<AGE_PROF_MIN||age>AGE_PROF_MAX){
		return false;
	}
	if(!gendre.compare(MALE)&&!gendre.compare(FEMALE)){
		return false;
	}
	if(this->professonals->isProfExist(id)){
		return false;
	}
	return true;
}

/********************************************************************
 * get the data of the movie from the user, check if legal, if legal*
 * 	create new movie with the get data, and add the new movie to the*
 * collection of the movie, if the data is illegal return FAILURE	*
 ********************************************************************/
Result Menu::newMovie(InputOutput* io){
	int len, year;
	double rate;
	string code,name,sum, imagePath;
	//get the all params of movie
	code = io->getNextString();
	name = io->getNextString();
	len = io->getNextInt();
	year = io->getNextInt();
	rate = io->getNextDouble();
	imagePath = io->getNextString();
	sum = io->getUpToEndLine();
	//Because its get the space that was before the string;
	if(!infoOfMovieIsLegal(code, len, year,rate)){ //check the data
		return FALIURE;
	}
	Movie * newMovie = new Movie(code, name, len, year, rate, imagePath,sum);
	this->movies->addMovie(newMovie); //add the new movie
	return SUCCSESS;
}

/********************************************************************
 * get the data of the prof from the user, check if legal, if legal	*
 * 	create new prof with the get data, and add the new prof to the	*
 * collection of the profs, if the data is illegal return FAILURE	*
 ********************************************************************/
Result Menu::newprof(InputOutput* io){
	int tp; //the type of the professional
	unsigned int id;
	int age;
	string name, des, gen;
	//get the all params of professional
	tp = io->getNextInt();
	id = io->getNextInt();
	age = io->getNextInt();
	des = io->getNextString();
	gen = io->getNextString();
	name = io->getUpToEndLine();
	if(!infoOfProfIsLegal(tp,id,age,gen)){ //check the data
		return FALIURE;
	};
	typeProf t = (typeProf)tp;
	Professional *newProf = this->profFact->create(t, id, name, des, age, gen); //create new prof
	this->professonals->addProf(newProf);//add
	return SUCCSESS;
}

/********************************************************************
 * get code of movie and id of prof, if both exist add the prof to 	*
 * 	the movie, and return success otherwise return faliure			*
 ********************************************************************/
Result Menu::addProfToMovie(InputOutput* io){
	unsigned int id;
	string code;
	code= io->getNextString();
	id = io->getNextUnsignedInt();
	//check if the input is legal
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	if(!this->professonals->isProfExist(id)){
		return FALIURE;
	}
	Professional* p = this->professonals->getProf(id);
	if(this->movies->isProfInMovie(p, code)){//if the prof is already in the movie personal
		return FALIURE;
	}
	this->movies->addProfToMovie(p, code);
	return SUCCSESS;
}

/********************************************************************
*  get the code of the movie and the name of the genre and send the	*
*   information to the movies collection           					*
*********************************************************************/
Result Menu::addMovieToGenre(InputOutput* io){
	string code, genre;
	code  = io->getNextString();
	genre = io->getNextString();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	if(this->movies->isGenreInMovie(genre, code)) {
		return FALIURE;
	}
	this->movies->addGenToMovie(genre, code);
	return SUCCSESS;
}

/********************************************************************
*  get the code of the movie and the type of the sort, and send the	*
*   information to the movies collection							*
*********************************************************************/
Result Menu::setSortProfForMovie(InputOutput* io){
	string code;
	int s; //sortType
	code = io->getNextString();
	s = io->getNextInt();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	if(s<SORT_PROF_MIN||s>SORT_PROF_MAX){
		return FALIURE;
	}
	SortProfType st = (SortProfType)s;
	this->movies->setSortProf(st,code);
	return SUCCSESS;
}

/*********************************************************************
*  get the code of the movie and send it to the movies collection	 *
**********************************************************************/
Result Menu::printProfOfMovie(InputOutput* io){
	string code;
	code = io->getNextString();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	this->movies->printProfOfMovie(code,io);
	return SUCCSESS_NO_PRINT;
}

/********************************************************************
*  get the code of the movie and send it to the movies collection 	*
*********************************************************************/
Result Menu::printMovie(InputOutput* io){
	string code;
	code= io->getNextString();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	this->movies->printMovie(code,io);
	return SUCCSESS_NO_PRINT;
}

/********************************************************************
*  get the all codes of the movies and send them in array to the 	*
*  	movies collection												*
*********************************************************************/
Result Menu::connectMovie(InputOutput* io){
	list<string> *codes = new list<string>;
	string code;
	string line = io->getUpToEndLine();
	line+=PARSER_BETWEEN_CODES;
	//add the codes to one set, if code enter more then once return failure
	istringstream is(line);
	while (getline(is,code,PARSER_BETWEEN_CODES)){
	    codes->push_back(code);
	}
	if(NULL ==codes){
		return FALIURE;
	}
	if(codes->size()<=1){
		delete codes;
		return FALIURE;
	}
	//check if the all codes are in the collection
	for(list<string>::iterator it = codes->begin();it!=codes->end();it++){
		string code = *it;
		if(!this->movies->isCodeExist(code)){
			delete codes;
			return FALIURE;
		}
	}
	this->movies->connectMovies(codes);
	delete codes;
	return SUCCSESS;
}

/*********************************************************************
*  get id of one professional, and send it to the match function in	 *
*  	 the professional collection, for print the movies of this		 *
*  professional												 		 *
**********************************************************************/
Result Menu::printMoviesOfProf(InputOutput* io){
	unsigned int id;
	id = io->getNextUnsignedInt();
	if(!this->professonals->isProfExist(id)){
		return FALIURE;
	}
	this->professonals->printMoviesOfProf(id,io);
	return SUCCSESS_NO_PRINT;
}

/*********************************************************************
*  get the code of the movie, and send it  to the match function in  *
*  	 the movies	collection, to be deleted this movie from the system,*
*  and there the movie will be deleted from the all professionals 	 *
*  that in his list											   		 *
**********************************************************************/
Result Menu::deleteMovie(InputOutput* io){
	string code;
	code = io->getNextString();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	this->movies->deleteMovie(code);
	return SUCCSESS;
}

/*********************************************************************
*  get the id of the professional, and send it  to the match function*
*  	 in the	professional collection, to be deleted from the system,  *
*  and there the professional will be deleted from the all movies	 *
*  that in his list										 			 *
**********************************************************************/
Result Menu::deleteProf(InputOutput* io){
	unsigned int id;
	id = io->getNextUnsignedInt();
	if(!this->professonals->isProfExist(id)){
		return FALIURE;
	}
	this->professonals->deleteProf(id);
	return SUCCSESS;
}

/*********************************************************************
*  get the id of the professional, and the code of the movie, and 	 *
*  	send it  to the match function in the movie collection, to delete*
*  the professional from movie										 *
**********************************************************************/
Result Menu::deleteProfFromMovie(InputOutput* io) {
	string code;
	unsigned int id;
	code = io->getNextString();
	id =io->getNextUnsignedInt();
	if(!this->movies->isCodeExist(code)){
		return FALIURE;
	}
	if(!this->professonals->isProfExist(id)){
		return FALIURE;
	}
	Professional *p = this->professonals->getProf(id);
	if(!this->movies->isProfInMovie(p,code)){
		return FALIURE;
	}
	this->movies->deleteProfFromMovie(p, code);
	return SUCCSESS;
}

/*********************************************************************
*  print the all movies that belong to the getting genre			 *
**********************************************************************/
Result Menu::printMoviesOfGenre(InputOutput* io){
	string gen;
	gen= io->getNextString();
	bool ret = this->movies->printMoviesOfGenre(gen,io);
	if(ret){
		return SUCCSESS_NO_PRINT;
	}else{
		return FALIURE;
	}
}

/*we had to initialize the static varibels, and when we did that in
 * the header file was compiler error */
Menu* Menu::instance = NULL;
bool Menu::created = false;
MutexLocker* Menu::lock = new MutexLocker();


/************************************************************
 * the start of the thread, if the menu doesn't create it	*
 * 	create it, with the lock and the if's, and then start 	*
 * the running of the order 								*
 ************************************************************/
void* Menu::startRunning(void*socket){
	if(!Menu::created){
		Menu::lock->Lock();
		if(!created){
			Menu::instance=new Menu();
			Menu::created=true;
		}
		Menu::lock->Unlock();
	}
	Menu::instance->start(*(int*)socket);
	return NULL;
}

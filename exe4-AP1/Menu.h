/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the menu run the 	*
* 	orders form the user, get the input *
* and send it to the match function		*
****************************************/

#ifndef MENU_H_
#define MENU_H_
#include<string>
#include <pthread.h>
#include"MutexLocker.h"
#include"InputOutput.h"
#include"MovieCollection.h"
#include"ProfessionalCollection.h"
#include"ProfessionalFactory.h"
using namespace std;

typedef enum{SUCCSESS, FALIURE ,SUCCSESS_NO_PRINT} Result;

/****************************************************************************
* the class run the option of the user, and get the all input from the 		*
* 	class of inputOutput, check the input and then send it to the match 	*
* functions in the collections												*
*****************************************************************************/
class Menu{
	private:
		static MutexLocker* lock;
		static bool created;
		static Menu* instance;
		MovieCollection* movies;
		ProfessionalCollection* professonals;
		ProfessionalFactory* profFact;
		/********************************************************************
		*  if the code is legal(dons't exist is the collection), and the	*
		*  	other info is legal too, return true, otherwise return false	*
		*********************************************************************/
		bool infoOfMovieIsLegal(string code, int len, int year, double rate);

		/********************************************************************
		*  if the id is legal(dons't exist is the collection), and the other*
		*  	info is legal too, return true, otherwise return false			*
		*********************************************************************/
		bool infoOfProfIsLegal(int tp,unsigned int id, int age, string gendre);

		/********************************************************************
		*  get the information of the movie, and add the movie to the 		*
		*  	collection                 										*
		*********************************************************************/
		Result newMovie(InputOutput* io);

		/********************************************************************
		* get the information of the professional, and add the prof to the 	*
		* 	collection           											*
		*********************************************************************/
		Result newprof(InputOutput* io);

		/********************************************************************
		* get the code of the movie and the id of the prof, get the prof 	*
		* 	from the collection, and send the code and the prof to the movie*
		* collection             											*
		*********************************************************************/
		Result addProfToMovie(InputOutput* io);

		/********************************************************************
		*  get the code of the movie and the name of the genre and send the	*
		*   information to the movies collection           					*
		*********************************************************************/
		Result addMovieToGenre(InputOutput* io);

		/********************************************************************
		*  get the code of the movie and the type of the sort, and send the	*
		*   information to the movies collection							*
		*********************************************************************/
		Result setSortProfForMovie(InputOutput* io);

		/*********************************************************************
		*  get the code of the movie and send it to the movies collection	 *
		**********************************************************************/
		Result printProfOfMovie(InputOutput* io);

		/********************************************************************
		*  get the code of the movie and send it to the movies collection 	*
		*********************************************************************/
		Result printMovie(InputOutput* io);

		/********************************************************************
		*  get the all codes of the movies and send them in array to the 	*
		*  	movies collection												*
		*********************************************************************/
		Result connectMovie(InputOutput* io);

		/*********************************************************************
		*  get id of one professional, and send it to the match function in	 *
		*  	 the professional collection, for print the movies of this		 *
		*  professional												 		 *
		**********************************************************************/
		Result printMoviesOfProf(InputOutput* io);

		/*********************************************************************
		*  get the code of the movie, and send it  to the match function in  *
		*  	 the movies	collection, to be deleted this movie from the system,*
		*  and there the movie will be deleted from the all professionals 	 *
		*  that in his list											   		 *
		**********************************************************************/
		Result deleteMovie(InputOutput* io);

		/*********************************************************************
		*  get the id of the professional, and send it  to the match function*
		*  	 in the	professional collection, to be deleted from the system,  *
		*  and there the professional will be deleted from the all movies	 *
		*  that in his list										 			 *
		**********************************************************************/
		Result deleteProf(InputOutput* io);

		/*********************************************************************
		*  get the id of the professional, and the code of the movie, and 	 *
		*  	send it  to the match function in the movie collection, to delete*
		*  the professional from movie										 *
		**********************************************************************/
		Result deleteProfFromMovie(InputOutput* io);

		/*********************************************************************
		*  print the all movies that belong to the getting genre			 *
		**********************************************************************/
		Result printMoviesOfGenre(InputOutput* io);

		/********************************************************************
		*  Create empty movies collection and empty professional 			*
		*  	collection, and the professional factory					  	*
		********************************************************************/
		Menu();

	public:

		/********************************************************************
		* delete the collection of movies and the collection of professional*
		*********************************************************************/
		~Menu();
		/********************************************************************
		*  start the program 											  	*
		*********************************************************************/
		void start(int socketNumber);

		/************************************************************
		 * start of the thread, create menu if needed and then start*
		 *  to run it												*
		 ************************************************************/
		static void* startRunning(void* socket);
};

#endif /* MENU_H_ */

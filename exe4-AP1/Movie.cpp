/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Movie, holds the 	*
* 	data and the personal of himself	*
****************************************/
#include"Movie.h"
#include"Professional.h"
#include"MoviesPersonal.h"
#include<iostream>
#define MERGE_CODES_PARSER "_"
/********************************************************************
 * delete the list of genres, and then delete						*
 ********************************************************************/
Movie::~Movie(){
	delete this->genres;
}

/********************************************************************
 * create new movie with the data from the input, and create the 	*
 * 	personal and the empty list of genres							*
 ********************************************************************/
Movie::Movie(string code, string name, int length, int year, double rank, string imagePath,string summary){
	this->code = code;
	this->name = name;
	this->length = length;
	this->year= year;
	this->rank =rank;
	this->image = imagePath;
	this->summary = summary;
	this->profs  = new MoviesPersonal();
	this->genres = new list<string*>;
}


/********************************************************************
 * print the all data of the movie, firstly the data and then the	*
 * 	personal														*
 ********************************************************************/
void Movie::print(InputOutput*io){
	ostringstream os;
	os<<this->getCode()<<" "<<this->getName()<<" "<<this->getLength()
						<<" "<<this->getYear()<<" "<<this->getRank()<<" ";
	io->print(os.str());
	list<string*>::iterator it;
	list<string*> genres = *(this->getCopyGenres());
	for(it = genres.begin(); it != genres.end();){
		string *gen = *it;
		io->print(*gen);
		if(++it != genres.end()){ //between te genres need
			io->print(",");
		} else{//after the last need space
			io->print(" ");
		}

	}
	io->print(this->getImagePath() + " ");
	io->print(this->getSummary());
	io->print("\n");
	this->profs->printAllProf(io);
	io->print("@");
}

/********************************************************************
 * connect two movies together("this" and "secMovie"), the code is	*
 * 	concatenation of the two codes with "_" between, and the other 	*
 * data is from the longest movie, the genres and personal in merge	*
 * of both, and return the new movie								*
 ********************************************************************/
Movie* Movie::operator+(Movie* secMovie){
	string newCode = this->getCode() +MERGE_CODES_PARSER+secMovie->getCode();
	Movie *longer;
	if(*this>secMovie){
		longer =this;
	} else{
		longer = secMovie;
	}
	Movie * newMovie = new Movie(newCode,longer->getName(),longer->getLength(),
			longer->getYear(), longer->getRank(), longer->getImagePath(),longer->getSummary());

	//merge the two list of genres and add them to the new movie
	list<string*> mergeGen = (*this->getCopyGenres());
	list<string*> secGenres = (*secMovie->getCopyGenres());
	list<string*>::iterator it;
	for(it= secGenres.begin(); it!=secGenres.end(); it++){
		string *gen = *it;
		if(!this->isGenreExistInMovie(*gen)){
			mergeGen.push_back(gen);
		}
	}
	for(it = mergeGen.begin(); it!=mergeGen.end(); it++){
		newMovie->addGenre(*(*it));
	}

	//merge the two list of professional and add them to the new movie
	list<Professional*> mergeProf = (*this->getCopyProf());
	list<Professional*> secProf = (*secMovie->getCopyProf());
	list<Professional*>::iterator itP;
	for(itP = secProf.begin(); itP!=secProf.end(); itP++){
		Professional * p = *itP;
		if(!this->isProfInMovie(p)){
			mergeProf.push_back(p);
		}
	}
	//add the all profs to the new movie
	for(itP= mergeProf.begin(); itP!=mergeProf.end(); itP++){
		newMovie->addProf(*itP);
	}
	return newMovie;
} //end of operator+

/********************************************************************
 * return true if "this" is longer or equal (in length) from sec,	*
 * 	otherwise return false											*
 ********************************************************************/
bool Movie::operator>(Movie* secMovie) {
	return (this->getLength() >= secMovie->getLength());
}

/********************************************************************
 * return true if "this" and the secMovie have the same code		*
 ********************************************************************/
bool Movie::operator ==(Movie* secMovie){
	if(this->getCode().compare(secMovie->getCode())){
		return false;
	}
	return true;
}

/********************************************************************
 * add the prof to the personal and add "this" to the prof			*
 ********************************************************************/
void Movie::addProf(Professional * prof){
	this->profs->addProf(prof);
	prof->addMovie(this);
}

/********************************************************************
 * if the get genre is't exist in the list, add it to the list		*
 ********************************************************************/
void Movie::addGenre(string gen){
	if(this->isGenreExistInMovie(gen)){ //check if exist
		return;
	}
	string* theGen = new string();
	*theGen =gen;
	this->genres->push_back(theGen); //add
}

/********************************************************************
 * print the all personal of the movie								*
 ********************************************************************/
void Movie::printAllProf(InputOutput*io){
	this->profs->printAllProf(io);
}

/********************************************************************
 * delete the prof from the personal								*
 ********************************************************************/
void Movie::deleteProf(Professional * prof){
	this->profs->deleteProf(prof, this);
}

/********************************************************************
 * set the type of the personal according to the input				*
 ********************************************************************/
void Movie::setProfSort(SortProfType profSort){
	this->profs->sort(profSort);
}

/********************************************************************
 * return the code													*
 ********************************************************************/
string Movie:: getCode() {
	return this->code;
}

/********************************************************************
 * return the name													*
 ********************************************************************/
string Movie::getName(){
	return this->name;
}

/********************************************************************
 * return the length													*
 ********************************************************************/
int Movie::getLength(){
	return this->length;
}

/********************************************************************
 * return the year													*
 ********************************************************************/
int Movie::getYear(){
	return this->year;
}

/********************************************************************
 * return the rank													*
 ********************************************************************/
double Movie::getRank(){
	return this->rank;
}

/********************************************************************
 * create new list of strings and copy to it the list of genres		*
 * 	return the new list												*
 ********************************************************************/
list<string*>* Movie::getCopyGenres() {
	list<string*>*  temp = new list<string*>;
	*temp = *(this->genres);
	return temp;
}

/********************************************************************
 * return the summary												*
 ********************************************************************/
string Movie::getSummary(){
	return this->summary;
}

/********************************************************************
 * create new list of Professional and copy to it the personal		*
 * 	return the new list												*
 ********************************************************************/
list<Professional*>* Movie::getCopyProf() {
	list<Professional*>* temp = new list<Professional*>;
	*temp = *(this->profs->getCopyProfs());
	return temp;
}

/********************************************************************
 * delete the list of genres, delete the personal and then "commit	*
 * 	suicide"														*
 ********************************************************************/
void Movie::deleteTheMovie(){
	for(list<string*>::iterator it = this->genres->begin();
			it!=this->genres->end();it++){
		string *s = *it;
		it = this->genres->erase(it);
		delete s; //delete the genre
	}
	this->profs->deletePersonal(this); //delete the personal
	delete this;
}

/********************************************************************
 * over the list of genres and check if exist genre like the input,	*
 * 	if exist return true, false otherwise							*
 ********************************************************************/
bool Movie::isGenreExistInMovie(string genre){
	list<string*> genres = *(this->getCopyGenres()); //get the list
	for(list<string*>::iterator it = genres.begin(); //over the list
			it!=genres.end(); it++){
		string* tempGen = *it;
		if(!tempGen->compare(genre)){ //check
			return true;
		}
	}
	return false; //nothing match
}

/********************************************************************
 * return true if prof exist in the personal						 *
 ********************************************************************/
bool Movie::isProfInMovie(Professional* prof){
	return this->profs->isProfInMovie(prof->getId());
}

/********************************************************************
 * return the image path											*
 ********************************************************************/
string Movie::getImagePath(){
	return this->image;
}

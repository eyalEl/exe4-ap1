/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5	              	*
* File description: Movie, holds the 	*
* 	data and the personal of himself	*
****************************************/


#ifndef MOVIE_H_
#define MOVIE_H_
#include<string>
#include<list>
using namespace std;
class Professional;
class MoviesPersonal;
class InputOutput;
#define SORT_PROF_MIN 1
#define SORT_PROF_MAX 3
typedef enum{SORT_PROF_ID = 1,SORT_PROF_AGE, SORT_PROF_MOVIES} SortProfType;
/********************************************************************
* the class is hold the all information on movie, and his personal	*
*********************************************************************/
class Movie{
	private:
		string code;
		string name;
		int length;
		int year;
		double rank;
		string image;
		list<string*>* genres;
		string summary;
		MoviesPersonal* profs;
		/******************************************************************
		* delete the every member that need to be deleted				  *
		*******************************************************************/
		~Movie();
	public:
		/********************************************************************
		* create new movie, and save the information that he gets			*
		*********************************************************************/
		Movie(string code, string name, int length, int year, double rank,
				string imagePath,string summary);

		/************************************************************************
		* connect two movies by create new movie that his information is from	*
		* 	 the longest one, and the genre and the personal in from them both	*
		*************************************************************************/
		Movie* operator+(Movie* secMovie);

		/********************************************************************
		* return true if this movie is longest from the secMovie, otherwise	*
		* 	return false   												 	*
		*********************************************************************/
		bool operator>(Movie* secMovie);

		/*********************************************************************
		* return true if secMovie and this movie is the same				 *
		**********************************************************************/
		bool operator==(Movie *secMovie);

		/********************************************************************
		* add the prof to the personal										*
		*********************************************************************/
		void addProf(Professional * prof);

		/********************************************************************
		* add the gen to the list of the genres						 		*
		*********************************************************************/
		void addGenre(string gen);

		/********************************************************************
		* print the movie										 			*
		*********************************************************************/
		void print(InputOutput*io);

		/********************************************************************
		* print the all personal								 			*
		*********************************************************************/
		void printAllProf(InputOutput*io);

		/********************************************************************
		*  send the prof to print, according to the id					 	*
		*********************************************************************/
		void deleteProf(Professional * prof);

		/********************************************************************
		* set the type of the sort of the personal					 		*
		*********************************************************************/
		void setProfSort(SortProfType profSort);

		/********************************************************************
		* get the code of the movie					   						*
		*********************************************************************/
		string getCode();

		/********************************************************************
		* get the name of the movie							 				*
		*********************************************************************/
		string getName();

		/********************************************************************
		* get the length of the movie(in minutes)						 	*
		*********************************************************************/
		int getLength();

		/********************************************************************
		* get the year of the movie							 				*
		*********************************************************************/
		int getYear();
		/********************************************************************
		* get the ramk of the movie							 				*
		*********************************************************************/
		double getRank();

		/********************************************************************
		* get the the genres of the movie								 	*
		*********************************************************************/
		list<string*>* getCopyGenres();

		/********************************************************************
		* get the summary of the movie							 			*
		*********************************************************************/
		string getSummary();

		/********************************************************************
		* get the list of the personal of the movie						 	*
		*********************************************************************/
		list<Professional*>* getCopyProf();

		/********************************************************************
		* first delete the movie from his personal, then delete the movie	*
		*********************************************************************/
		void deleteTheMovie();

		/********************************************************************
		 * return true if genre is in the movie , false otherwise 			*
		 *********************************************************************/
		bool isGenreExistInMovie(string genre);

		/********************************************************************
		* return true if the professional in the list of the Professionals	*
		* 	, otherwise return false.										*
		*********************************************************************/
		bool isProfInMovie(Professional* prof);

		/********************************************************************
		 * return the image path											*
		 ********************************************************************/
		string getImagePath();
};

#endif /* MOVIE_H_ */

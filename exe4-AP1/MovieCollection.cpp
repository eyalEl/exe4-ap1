/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: collection of the	*
* 	all movie that in the system		*
****************************************/
#include"MovieCollection.h"
#include<iostream>

/********************************************************************************
 * over the all list of movies, and if one of there code match to the code that	*
 * 	received as input, return the match movie, otherwise return false			*
 ********************************************************************************/
Movie* MovieCollection::getMovie(string code){
	list<Movie*>::iterator it;
	//over the all list
	for(it= this->movies->begin();it!=this->movies->end();it++){
		Movie* temp  =*it;
		if(!temp->getCode().compare(code)){//if the code match return the movie
			return temp;
		}
	}
	return NULL;
}

/********************************************************************************
 * c'tor, create empty list														*
 ********************************************************************************/
MovieCollection::MovieCollection(){
	this->movies = new list<Movie*>;
}
/********************************************************************************
 * d'tor, delete the all movies first											*
 ********************************************************************************/
MovieCollection::~MovieCollection(){
	list<Movie*>::iterator it;
	for(it = this->movies->begin();it!=this->movies->end();){
		Movie *m =*it;
		it = this->movies->erase(it);
		m->deleteTheMovie();
	}
	delete this->movies;
}
/********************************************************************************
 * over the all list of the movies and delete them, one by one					*
 ********************************************************************************/
void MovieCollection::deleteAllMovies(){
	list<Movie*>::iterator it;
	//over the all list
	for(it= this->movies->begin();it!=this->movies->end();){
		Movie*temp = *it;
		it = this->movies->erase(it);
		temp->deleteTheMovie();
	}
}

/********************************************************************************
 * add movie to the end of the list												*
 ********************************************************************************/
void MovieCollection::addMovie(Movie * newMovie){
	this->movies->push_back(newMovie);
}

/********************************************************************************
 * delete the movie that the movieCode belong to, from the list, and then delete*
 * 	the real movie 																*
 ********************************************************************************/
void MovieCollection::deleteMovie(string movieCode){
	Movie* theMovie = this->getMovie(movieCode);
	if(theMovie!=NULL){
		this->movies->remove(theMovie);
		theMovie->deleteTheMovie();
	}
}

/********************************************************************************
 * print the movie that the movieCode belong to									*
 ********************************************************************************/
void MovieCollection::printMovie(string movieCode,InputOutput *io){
	Movie* theMovie = this->getMovie(movieCode);
	if(theMovie!=NULL){
		theMovie->print(io);
	}
}

/********************************************************************************
 * over the all list of the movies and print one by one							*
 ********************************************************************************/
void MovieCollection::printAllMovies(InputOutput *io){
	list<Movie*>::iterator it;
	//over the all list
	for(it = this->movies->begin();it!=this->movies->end();it++){
		Movie* temp  =*it;
		temp->print(io);
	}
}
/********************************************************************************
 * over the set that in the input, and connect them together , if there is just *
 * 	one code or less return							 							*
 ********************************************************************************/
void MovieCollection::connectMovies(list<string> *moviesCode){
	Movie * theNewMovie;
	//there isn't enough movies to connect
	if(moviesCode->size()<=1) {
		return;
	}
	//over the codes
	list<string>::iterator it = moviesCode->begin();
	theNewMovie = this->getMovie(*it);
	Movie* first = theNewMovie;
	for(it++;it!=moviesCode->end();){
		Movie * sec = this->getMovie(*it);
		Movie *temp= (*theNewMovie) + sec;
		it++;
		if(theNewMovie->getCode()!=first->getCode() && it!=moviesCode->end()){
			theNewMovie->deleteTheMovie(); //delete the all movies that create in the process
		}
		theNewMovie = temp;
	}
	this->addMovie(theNewMovie);
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist add the prof to it		*
 ********************************************************************************/
void MovieCollection::addProfToMovie(Professional * prof, string movieCode){
	Movie* theMovie = this->getMovie(movieCode);
	if(NULL!=theMovie){
		theMovie->addProf(prof);
	}
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist add the gen to it		*
 ********************************************************************************/
void MovieCollection::addGenToMovie(string gen, string movieCode){
	Movie * theMovie = this->getMovie(movieCode);
	if(NULL== theMovie){
		return;
	}
	theMovie->addGenre(gen);
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist add the gen to it		*
 ********************************************************************************/
void MovieCollection::deleteProfFromMovie(Professional * prof, string movieCode){
	Movie * theMovie = this->getMovie(movieCode);
	if(NULL== theMovie){
		return;
	}
	theMovie->deleteProf(prof);

}

/********************************************************************************
 * if the movie of the "movieCode" exist, set his professional sort to the 		*
 * 	get type																	*
 ********************************************************************************/
void MovieCollection::setSortProf(SortProfType sortType, string movieCode){
	Movie * theMovie = this->getMovie(movieCode);
	if(NULL== theMovie){ //check if realy a movie
		return;
	}
	theMovie->setProfSort(sortType); //set the sort
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist print his personal		*
 ********************************************************************************/
void MovieCollection::printProfOfMovie(string code, InputOutput *io){
	Movie * theMovie = this->getMovie(code);
	if(NULL== theMovie){//check if exist
		return;
	}
	theMovie->printAllProf(io); //print the personal
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist return true, otherwise	*
 * 	return false																*
 ********************************************************************************/
bool MovieCollection::isCodeExist(string code){
	Movie* theMovie  = this->getMovie(code);
	if(NULL==theMovie){
		return false;
	}
	return true;
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist check if the get genre 	*
 * 	is exist in the movie, if exist return ture, otherwise return false			*
 ********************************************************************************/
bool MovieCollection::isGenreInMovie(string genre, string code){
	Movie* movie = getMovie(code); //get the movie
	if(NULL==movie){
		return false;
	}
	return movie->isGenreExistInMovie(genre); //check if the genre exist
}

/********************************************************************************
 * over the all list of movies and check who has hold genre like the input and	*
 * 	print them																	*
 ********************************************************************************/
bool MovieCollection::printMoviesOfGenre(string genre, InputOutput *io){
	bool wasGen = false;
	list<Movie*>::iterator it;
	for(it = this->movies->begin(); it!=this->movies->end();it++){
		Movie *temp = *it;
		if(this->isGenreInMovie(genre,temp->getCode())){ //if true print
			temp->print(io);
			wasGen = true;
		}
	}
	return wasGen;
}

/********************************************************************************
 * get the movie that match to "movieCode" and if exist check if the get prof	*
 * 	exist in the movie, return true if exist, otherwise false					*
 ********************************************************************************/
bool MovieCollection::isProfInMovie(Professional* prof, string codeMovie){
	Movie* theMovie = this->getMovie(codeMovie);
	if(theMovie){
		return theMovie->isProfInMovie(prof);
	}
	return false;
}

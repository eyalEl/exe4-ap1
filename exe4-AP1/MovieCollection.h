/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: collection of the	*
* 	all movie that in the system		*
****************************************/

#ifndef MOVIECOLLECTION_H_
#define MOVIECOLLECTION_H_
#include"Movie.h"
#include"Professional.h"
#include"InputOutput.h"
#include<string>
#include<set>
#include<list>
using namespace std;
/********************************************************************
*  the class in charge of holding the all movies, print them, delete*
*	 them , and send to each movie the match information, according	*
*  what send to it	                								*
*********************************************************************/
class MovieCollection{
	private:
		/********************************************************************
	 	 * delete the all movies that in the list							*
	 	 ********************************************************************/
		void deleteAllMovies();

		list<Movie*>* movies;
	public:
		/********************************************************************
		*  Create empty list of movies						                *
		*********************************************************************/
		MovieCollection();

		/********************************************************************
		*  erase the all list of movies							 			*
		*********************************************************************/
		~MovieCollection();

		/********************************************************************
		 *  the function get code of movie, and return the match movie of the*
		 *   list, if there isn't match movie return null					*
		 *********************************************************************/
		Movie* getMovie(string code);

		/********************************************************************
		*  add the newMovie to the list, the function assume that the movie	*
		*  	is legal to add to the list							 			*
		*********************************************************************/
		void addMovie(Movie * newMovie);

		/********************************************************************
		* erase the match movie to the movieCode from the list				*
		*********************************************************************/
		void deleteMovie(string movieCode);

		/********************************************************************
		* print the all data of the movie, that match to movieCode, line by *
		* 	line		  													*
		*********************************************************************/
		void printMovie(string movieCode,InputOutput *io);

		/********************************************************************
		* over the all list of the movie and print them one by one			*
		*********************************************************************/
		void printAllMovies(InputOutput *io);

		/********************************************************************
		* the function add to the list new movie that all his data belong to*
		*  	the longest movie from the array, and add to him the genre and 	*
		* the professionals from the other movies that in the array			*
		*********************************************************************/
		void connectMovies(list<string> *moviesCode);

		/********************************************************************
		* find the movie that match to movieCode form the list, and then add*
		*  	the prof to it.									 				*
		*********************************************************************/
		void addProfToMovie(Professional * prof, string movieCode);

		/********************************************************************
		* find the movie that match to movieCode form the list, and then add*
		*  	the genre to it.												*
		*********************************************************************/
		void addGenToMovie(string gen, string movieCode);

		/**********************************************************************
		* find the movie that match to movieCode form the list, and delete the*
		* 	professional that match to profId from it.				          *
		***********************************************************************/
		void deleteProfFromMovie(Professional * prof, string movieCode);

		/********************************************************************
		* find the movie that match to movieCode form the list, set the type*
		*	of the sort of his professional in the list						*
		*********************************************************************/
		void setSortProf(SortProfType sortType, string movieCode);

		/********************************************************************
		* find the movie that match to movieCode form the list, and print 	*
		* 	all of the professional that belong to him						*
		*********************************************************************/
		void printProfOfMovie(string code, InputOutput *io);

		/********************************************************************
		* return true if the code is exist in the list, false otherwise		*
		*********************************************************************/
		bool isCodeExist(string code);

		/********************************************************************
		* return true if genre is in the movie that the code belong too		*
		* , false otherwise, the function assumes the code is exist 		*
		*********************************************************************/
		bool isGenreInMovie(string genre, string code);

		/********************************************************************
		* print the all movies that belong to this genre					*
		* 	return true, if was movie with the get genre , false otherwise	*
		*********************************************************************/
		bool printMoviesOfGenre(string genre, InputOutput *io);

		/********************************************************************
		* return true if the movie have the get prof in his personal		*
		*********************************************************************/
		bool isProfInMovie(Professional* prof, string codeMovie);
};

#endif /* MOVIECOLLECTION_H_ */

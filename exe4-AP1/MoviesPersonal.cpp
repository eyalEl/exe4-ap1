/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: collection of the	*
* 	all movie that in the system		*
****************************************/

#include"MoviesPersonal.h"
#include<algorithm>

/********************************************************************
* over the list, return true if there is a professional that his id	*
* 	is match to the id that in the input							*
*********************************************************************/
bool MoviesPersonal::isProfInMovie(unsigned int id) {
	list<Professional*>::iterator it;
		for(it = this->professionals->begin(); //over the list
				it!=this->professionals->end(); it++){
			if((*it)->getId()==id){ //check if match
				return true;
		}
	}
	return false; //nothing match
}

/********************************************************************
* delete the personal, and the list and the compare logic			*
*********************************************************************/
MoviesPersonal::~MoviesPersonal(){
	delete this->professionals;
	delete this->currentSort;
}

/********************************************************************
* create the personal, and put compare by id as default compare		*
*********************************************************************/
MoviesPersonal::MoviesPersonal(){
	this->currentSort = new CompareFunc();
	this->currentSort->setTypeCompare(new CompareId());
	this->professionals = new list<Professional*>;
}

/********************************************************************
* delete the movie from the personal and then "commit suicide"		*
*********************************************************************/
void MoviesPersonal::deletePersonal(Movie * theMovie){
	this->deleteMovieFromPersonal(theMovie);
	this->professionals->clear();
	delete this;
}

/********************************************************************
* if the prof dons't exist in the list, add them to the end and then*
* 	sort the list according to the the compare logic 				*
*********************************************************************/
void MoviesPersonal::addProf(Professional * prof){
	if(this->isProfInMovie(prof->getId())){//check if exist
		return;
	}
	this->professionals->push_back(prof);//add
	this->professionals->sort(*(this->currentSort));//sort
}

/********************************************************************
* over the list of the professional and print them one by one		*
*********************************************************************/
void MoviesPersonal::printAllProf(InputOutput*io){
	list<Professional*>::iterator it;
	for(it = this->professionals->begin();
			it!=this->professionals->end(); it++){
		Professional* tempP = *it;
		tempP->print(io);
	}
}

/********************************************************************
* delete the prof form the list, and print the list from the prof	*
*********************************************************************/
void MoviesPersonal::deleteProf(Professional * prof, Movie* thisMovie){
	if(!this->isProfInMovie(prof->getId())){//check if the prof realy exist
		return;
	}
	this->professionals->remove(prof); //remove from the list
	prof->deleteMovie(thisMovie); //remove the movie from the prof
}

/********************************************************************
* according to the input set the compare logic, and sort the list	*
*********************************************************************/
void MoviesPersonal::sort(SortProfType typeSort){
	switch(typeSort){ //set the compare logic
	case(SORT_PROF_ID):
			this->currentSort->setTypeCompare(new CompareId());
			break;
	case(SORT_PROF_AGE):
			this->currentSort->setTypeCompare(new CompareAge());
			break;
	case(SORT_PROF_MOVIES):
			this->currentSort->setTypeCompare(new CompareMovies());
			break;
	default:
		break;
	}
	this->professionals->sort(*this->currentSort);//sort
}

/********************************************************************
* delete the movie from the all list of professional , and delete 	*
* 	the list														*
*********************************************************************/
void MoviesPersonal::deleteMovieFromPersonal(Movie * movie){
	list<Professional*>::iterator it;
	for(it = this->professionals->begin();
			it!=this->professionals->end();){
		Professional* tempP = *it;
		it = this->professionals->erase(it);
		tempP->deleteMovie(movie);
	}
}

/********************************************************************
* create new list of Professional and copy the list to it, and 		*
* 	return the new list
*********************************************************************/
list<Professional*>* MoviesPersonal::getCopyProfs() {
	list<Professional*>*temp = new list<Professional*>; //create
	*temp = *(this->professionals); //copy
	return temp;
}

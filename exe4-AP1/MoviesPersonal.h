/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: holds the all 		*
* 	personal on one movie				*
****************************************/

#ifndef MOVIESPERSONAL_H_
#define MOVIESPERSONAL_H_
#include"Movie.h"
#include"Professional.h"
#include "CompareFunc.h"
#include "CompareMovies.h"
#include "CompareAge.h"
#include "CompareId.h"
#include<list>
using namespace std;
/********************************************************************
* the class contain the all professional of movie					*
*********************************************************************/
class MoviesPersonal{

	private:
		list<Professional*>* professionals;
		 CompareFunc *currentSort;

		/********************************************************************
		* delete the personal 												*
		*********************************************************************/
		~MoviesPersonal();
	public:

		/********************************************************************
		* create the list of the professional								*
		*********************************************************************/
		MoviesPersonal();

		/********************************************************************
		* return true if the professional that belong to the id in the list	*
		* 	true, otherwise return false.									*
		*********************************************************************/
		bool isProfInMovie(unsigned int id);

		/********************************************************************
		* delete the movie from the all profession that in the list			*
		********************************************************************/
		void deletePersonal(Movie * theMovie);

		/********************************************************************
		* add prof to the list of the professional							*
		*********************************************************************/
		void addProf(Professional * prof);

		/********************************************************************
		* print the all professional that in the list						*
		*********************************************************************/
		void printAllProf(InputOutput*io);

		/********************************************************************
		* remove the prof form the list							 			*
		*********************************************************************/
		void deleteProf(Professional * prof, Movie* thisMovie);

		/********************************************************************
		* sort the list of the professional according the type of the sort	*
		*********************************************************************/
		void sort(SortProfType typeSort);

		/********************************************************************
		* remove the movie from the list of the movie of the all personal	*
		*********************************************************************/
		void deleteMovieFromPersonal(Movie * movie);

		/********************************************************************
		* return the list of the professional 						 		*
		*********************************************************************/
		list<Professional*>* getCopyProfs();

};

#endif /* MOVIESPERSONAL_H_ */

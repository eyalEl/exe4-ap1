/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim*
* Exercise Name: exe5               	*
* File description: the lock for the 	*
*  	singlesont							*
****************************************/

#include "MutexLocker.h"
#include <cstdio>

/************************************************************
* destroy the mutex											*
************************************************************/
MutexLocker::~MutexLocker() {
	pthread_mutex_destroy(&this->mutex);
}

/************************************************************
 * initialize the mutex										*
 ************************************************************/
MutexLocker::MutexLocker() {
	if(pthread_mutex_init(&this->mutex,NULL)!=0){
		perror("init lock probelm");
	}
}

/************************************************************
 * lock the mutex											*
 ************************************************************/
void MutexLocker::Lock(){
	pthread_mutex_lock(&this->mutex);
}

/************************************************************
 * unlock the mutex											*
 ************************************************************/
void MutexLocker::Unlock(){
	pthread_mutex_unlock(&this->mutex);
}

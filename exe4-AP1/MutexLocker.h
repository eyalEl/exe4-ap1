/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim*
* Exercise Name: exe5               	*
* File description: the lock for the 	*
*  	singlesont							*
****************************************/

#ifndef MUTEXLOCKER_H_
#define MUTEXLOCKER_H_
#include <pthread.h>

/************************************************************
 * class that hold the lock and handle it					*
 ************************************************************/
class MutexLocker {
private:
	pthread_mutex_t mutex;
public:
	/************************************************************
	 * lock the mutex											*
	 ************************************************************/
	void Lock();
	/************************************************************
	 * unlock the mutex											*
	 ************************************************************/
	void Unlock();
	/************************************************************
	 * destroy the mutex										*
	 ************************************************************/
	~MutexLocker();
	/************************************************************
	 * initialize the mutex										*
	 ************************************************************/
	MutexLocker();
};

#endif /* MUTEXLOCKER_H_ */

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Player, type of 	*
* 	professional						*
****************************************/

#include"Player.h"
#include<iostream>
/********************************************************************
 * create new player												*
 ********************************************************************/
Player::Player(unsigned int id, string name, string description, int age, string gen)
:Professional(id, name, description, age, gen){}

/********************************************************************
 * print the Player, his name and age								*
 ********************************************************************/
void Player::print(InputOutput*io){
	io->print(this->getName()+" ");
	io->print(this->getAge());
	io->print("\n");
}

/********************************************************************
* delete the Player													*
*********************************************************************/
Player::~Player(){}

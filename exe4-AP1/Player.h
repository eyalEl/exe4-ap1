/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Player, type of 	*
* 	professional						*
****************************************/

#ifndef PLAYER_H_
#define PLAYER_H
#include"Professional.h"
#include<string>
using namespace std;
/****************************************************************
* the class of the player										*
*****************************************************************/
class Player: public Professional{
	public:
		/********************************************************************
		 * create new player												*
		 ********************************************************************/
		Player(unsigned int id, string name, string description, int age, string gen);
		/********************************************************************
		* print the information that producer need to print when the user	*
		* 	 ask for														*
		*********************************************************************/
		void print(InputOutput*io);
		/********************************************************************
		* delete the Player													*
		*********************************************************************/
		~Player();
};
#endif /* PLAYER_H_ */

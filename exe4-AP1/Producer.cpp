/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Producer, type of 	*
* 	professional						*
****************************************/


#include"Producer.h"
#include<iostream>
/********************************************************************
 * create new producer												*
 ********************************************************************/
Producer::Producer(unsigned int id, string name, string description, int age, string gen) :
Professional(id, name, description, age, gen){}

/********************************************************************
 * print the producer, his name 									*
 ********************************************************************/
void Producer::print(InputOutput*io){
	io->print(this->getName());
	io->print("\n");
}

/********************************************************************
 * delete the producer												*
 *********************************************************************/
Producer::~Producer(){}

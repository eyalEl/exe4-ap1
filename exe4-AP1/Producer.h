/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Producer, type of 	*
* 	professional						*
****************************************/

#ifndef PRODUCER_H_
#define PRODUCER_H_
#include"Professional.h"
#include<string>
using namespace std;
/****************************************************************
* the class of the producer										*
*****************************************************************/
class Producer: public Professional
{
	public:
		/********************************************************************
		 * create new producer												*
		 ********************************************************************/
		Producer(unsigned int id, string name, string description, int age, string gen);
		/********************************************************************
		* print the information that producer need to print when the user	*
		* 	 ask for														*
		*********************************************************************/
		void print(InputOutput*io);

		/********************************************************************
		 * delete the producer												*
		*********************************************************************/
		~Producer();
};




#endif /* PRODUCER_H_ */

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5	              	*
* File description: holds the data on 	*
* 	the professional and his movies		*
****************************************/
#include"Professional.h"
#include<iostream>

/********************************************************************
 *create new professional with the data that in the input, andcreate*
 *	empty list of movies											*
 ********************************************************************/
Professional::Professional(unsigned int id, string name, string description, int age, string gen){
	this->id = id;
	this->age =age;
	this->name = name;
	this->jobDescrip = description;
	this->gender = gen;
	this->myMovies = new list<Movie*>;
}

/********************************************************************
 * distractor													 	*
 ********************************************************************/
Professional::~Professional(){
	delete this->myMovies;
}

/********************************************************************
 * if the movie dons't exist in the professional add the movie to 	*
 * 	the end of the list of the movies								*
 ********************************************************************/
void Professional::addMovie(Movie * theMovie){
	if(isMovieExistInProf(theMovie)){ //check if exist
		return;
	}
	this->myMovies->push_back(theMovie);
}

/********************************************************************
 * if the movie exist in the list, remove him from it				*
 ********************************************************************/
void Professional::deleteMovie(Movie * theMovie) {
	if(isMovieExistInProf(theMovie)){
		this->myMovies->remove(theMovie);
	}
}

/********************************************************************
 * over the all list of movies and return true if one of the movies	*
 * 	equal to the movie that in the input						 	*
 ********************************************************************/
bool Professional::isMovieExistInProf(Movie* movie){
	list<Movie*>::iterator itM;
	for(itM=this->myMovies->begin();
			itM!=this->myMovies->end(); itM++){
		if(*(*itM)==movie){
			return true;
		}
	}
	return false; //no one of the movies is match
}

/********************************************************************
 * return the id												 	*
 ********************************************************************/
unsigned int Professional::getId() {
	return this->id;
}

/********************************************************************
 * return the name												 	*
 ********************************************************************/
string Professional::getName(){
	return this->name;
}

/********************************************************************
 * return the job description									 	*
 ********************************************************************/
string Professional::getJobDescrip(){
	return this->jobDescrip;
}

/********************************************************************
 * return the gendre											 	*
 ********************************************************************/
string Professional::getGender(){
	return this->gender;
}

/********************************************************************
 * return the age												 	*
 ********************************************************************/
int Professional::getAge(){
	return this->age;
}

/********************************************************************
 * return true if the id of "this" is smaller then the secProf id 	*
 ********************************************************************/
bool Professional::operator<(Professional * secProf) {
	return (this->getId()<secProf->getId());
}

/********************************************************************
 * return true if the id's of "this" and secProf is the same	 	*
 ********************************************************************/
bool Professional::operator==( Professional *secProf) {
	return this->getId()==secProf->getId();
}

/********************************************************************
 * return the number of movies that in the list of the movies of the*
 * 	professional													*
 ********************************************************************/
int Professional::getNumberOfMovies(){
	return this->myMovies->size();
}

/********************************************************************
 * create new list of movies and copy the list to it, and return the*
 * 	new list(new place in the heap, same values)					*
 ********************************************************************/
list<Movie*>* Professional::getCopyMovies(){
	list<Movie*>* emp = new list<Movie*>;
	*emp = *myMovies; //copy
	return emp;
}

/********************************************************************
 * delete the prof from the all movies he belong to, and then delete*
 * 	himself(commit suicide)											*
 ********************************************************************/
void Professional::deleteTheProf(){
	list<Movie*>::iterator itM;
	for(itM=this->myMovies->begin();
			itM!=this->myMovies->end();){
		Movie *m  =*itM;
		itM = this->myMovies->erase(itM); //remove from the list
		m->deleteProf(this); //remove "this" from the movie
	}
	this->myMovies->clear();
	delete this;
}

/********************************************************************
 * print the all movies that belong to "this" professional		 	*
 ********************************************************************/
void Professional::printMovies(InputOutput*io){
	list<Movie*>::iterator itM;
	for(itM=this->myMovies->begin(); //over the list
			itM!=this->myMovies->end();itM++){
		(*itM)->print(io); //print
	}
}

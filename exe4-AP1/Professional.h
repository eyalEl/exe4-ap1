/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: holds the data on 	*
* 	the professional and his movies		*
****************************************/

#ifndef PROFESSIONAL_H_
#define PROFESSIONAL_H_
#include"Movie.h"
#include"InputOutput.h"
#include<string>
#include<list>
using namespace std;

#define FEMALE "female"
#define MALE "male"

/********************************************************************
* the class holds the information of  professional					*
*********************************************************************/
class Professional{
protected:
		unsigned int id;
		string name;
		string jobDescrip;
		int age;
		string gender;
		/********************************************************************
		* create new professional											*
		*********************************************************************/
		Professional(unsigned int id, string name, string description, int age, string gen);
	private:
		list<Movie*>* myMovies;

		/********************************************************************
		* return true if the movie exist in the list of the movie, false 	*
		* 	otherwise
		*********************************************************************/
		bool isMovieExistInProf(Movie* movie);

	public:
		/********************************************************************
		* delete the professional											*
		*********************************************************************/
		virtual ~Professional();
		/********************************************************************
		* add theMovie to the list of the movies							*
		*********************************************************************/
		void addMovie(Movie * theMovie);

		/********************************************************************
		* delete theMovie from the list 									*
		*********************************************************************/
		void deleteMovie(Movie * theMovie);

		/********************************************************************
		* tell the son to print himself										*
		*********************************************************************/
		virtual void print(InputOutput*io)=0;

		/********************************************************************
		* return the id														*
		*********************************************************************/
		unsigned int getId();

		/********************************************************************
		* return the name													*
		*********************************************************************/
		string getName();

		/********************************************************************
		* return the job description										*
		*********************************************************************/
		string getJobDescrip();

		/********************************************************************
		* return the gender 												*
		*********************************************************************/
		string getGender();

		/********************************************************************
		* return the age													*
		*********************************************************************/
		int getAge();

		/********************************************************************
		* if the id of "this" smaller then secProf return true, otherwise	*
		* 	return true														*
		*********************************************************************/
		bool operator<(Professional * secProf);

		/********************************************************************
		* if "this" have the same data like secProf, and if they have the 	*
		* 	same movies return true, otherwise return false					*
		*********************************************************************/
		bool operator==(Professional *secProf);

		/********************************************************************
		* get the number of the movies that the professional belong to		*
		*********************************************************************/
		int getNumberOfMovies();

		/********************************************************************
		* get the movies that the professional belong to					*
		*********************************************************************/
		list<Movie*>* getCopyMovies();

		/********************************************************************
		* firstly delete the professional from the all movies he in them, 	*
		* 	and them delete himself											*
		********************************************************************/
		void deleteTheProf();
		/*******************************************************************
		 * print the all movies of this professional					   *
		 *******************************************************************/
		void printMovies(InputOutput*io);
};

#endif /* PROFESSIONAL_H_ */

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: holds the all 		*
* 	professional that in the system		*
****************************************/
#include"ProfessionalCollection.h"


/********************************************************************
 *create empty list of Professional								 	*
 ********************************************************************/
ProfessionalCollection::ProfessionalCollection(){
	this->professionals = new list<Professional*>;
}

/********************************************************************
 *over the all list of all list professional and delete them one by	*
 *	one															 	*
 ********************************************************************/
ProfessionalCollection::~ProfessionalCollection(){
	list<Professional*>::iterator itP;
	for(itP = this->professionals->begin(); //over the list
			itP != this->professionals->end();){
		Professional *temp = *itP;
		itP = this->professionals->erase(itP); //delete from the list
		temp->deleteTheProf(); //delete the prof himself
	}
	delete this->professionals; //delete the list
}

/********************************************************************
 *add professional to the end of the list, if he is't exist before	*
 ********************************************************************/
void ProfessionalCollection:: addProf(Professional * newProf){
	if(this->isProfExist(newProf->getId())){ //check if exist before
		return;
	}
	this->professionals->push_back(newProf);
}

/********************************************************************
 *get the match prof(if exist) and print all of his movies		 	*
 ********************************************************************/
void ProfessionalCollection:: printMoviesOfProf(unsigned int id, InputOutput* io){
	Professional * p = this->getProf(id);
	if(NULL!= p){
		p->printMovies(io);
	}
}

/********************************************************************
 *over the all list of the professional and print them one by one 	*
 ********************************************************************/
void ProfessionalCollection:: printAllProf(InputOutput* io){
	list<Professional*>::iterator itP;
	for(itP = this->professionals->begin();
			itP != this->professionals->end(); itP++){
		(*itP)->print(io);
	}

}

/********************************************************************
 *get the match prof from the list, 		 	*
 ********************************************************************/
void ProfessionalCollection:: deleteProf(unsigned int id){
	Professional* p = this->getProf(id);
	if(NULL== p){
		return;
	}
	this->professionals->remove(p);
	p->deleteTheProf();
}
/********************************************************************
 *return the professional that have the same is like the id, that in*
 *	the input, otherwise return NULL							 	*
 ********************************************************************/
Professional* ProfessionalCollection::getProf(unsigned int id){
	list<Professional*>::iterator itP;
	for(itP = this->professionals->begin();
			itP != this->professionals->end(); itP++){
		if(id==(*itP)->getId()){ //the id's match
			return (*itP);
		}
	}
	return NULL;//if the all list didn't match
}

/********************************************************************
 *check if exist prof with the same id like the id that in the input*
 ********************************************************************/
bool ProfessionalCollection::isProfExist(unsigned int id){
	Professional* p = this->getProf(id);
	if(NULL== p){
		return false;
	}
	return true;
}

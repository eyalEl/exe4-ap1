/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: holds the all 		*
* 	professional that in the system		*
****************************************/

#ifndef PROFESSIONALCOLLECTION_H_
#define PROFESSIONALCOLLECTION_H_
#include"Professional.h"
#include"InputOutput.h"
#include<list>
using namespace std;
/****************************************************************
* the class contain the all professionals						*
*****************************************************************/
class ProfessionalCollection{
	private :
		list<Professional*>* professionals;
	public:
		/********************************************************************
		* create the list of the professional								*
		*********************************************************************/
		ProfessionalCollection();
		/********************************************************************
		* delete the all professional that In the list, and then delete		*
		* 	 himself											 		  	*
		*********************************************************************/
		~ProfessionalCollection();

		/********************************************************************
		* add the newProf to the list										*
		*********************************************************************/
		void addProf(Professional * newProf);

		/*******************************************************************
		* print the all movies of the professional that his id it is the id*
		*******************************************************************/
		void printMoviesOfProf(unsigned int id, InputOutput* io);

		/********************************************************************
		* print the all professionals										*
		*********************************************************************/
		void printAllProf(InputOutput* io);

		/********************************************************************
		* delete the professional that his id is the id, from the list and	*
		*  then the prof himself											*
		********************************************************************/
		void deleteProf(unsigned int id);

		/********************************************************************
		* return the professional that belong to the id					 	*
		*********************************************************************/
		Professional* getProf(unsigned int id);

		/********************************************************************
		* return true if exist prof with this id in the collection, false	*
		* 	otherwise													 	*
		*********************************************************************/
		bool isProfExist(unsigned int id);
};



#endif /* PROFESSIONALCOLLECTION_H_ */

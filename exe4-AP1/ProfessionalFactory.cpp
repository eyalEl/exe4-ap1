/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Player, type of 	*
* 	professional						*
****************************************/

#include"ProfessionalFactory.h"
/********************************************************************
 * according to the type in the input, create the match professioanl*
 * 	with the information that get too								*
 ********************************************************************/
Professional* ProfessionalFactory::create(typeProf type, unsigned int id,
		string name, string description, int age, string gen){
	switch(type){
	case(DIRECTOR):
		return new Director(id,name, description,age,gen);
		break;
	case(PLAYER):
		return new Player(id, name, description,age, gen);
		break;
	case(SCREENWRITER):
		return new ScreenWriter(id, name, description,age, gen);
		break;
	case(PRODUCER):
		return new Producer(id, name, description,age, gen);
		break;
	default:
		break;
	}

	return NULL;
}


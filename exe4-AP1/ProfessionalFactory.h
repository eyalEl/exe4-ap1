/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: create Professional	*
* 	according to the input in the create*
* function								*
****************************************/

#ifndef PROFESSIONALFACTORY_H_
#define PROFESSIONALFACTORY_H_
#include"Professional.h"
#include"ScreenWriter.h"
#include"Producer.h"
#include"Player.h"
#include"Director.h"

typedef enum{PROF_TYPE_MIN, DIRECTOR = PROF_TYPE_MIN, PLAYER, SCREENWRITER,
	PROF_TYPE_MAX ,PRODUCER = PROF_TYPE_MAX}typeProf;
#include<string>
using namespace std;
/********************************************************************
* the class create new professional according the string of the type*
*********************************************************************/
class ProfessionalFactory{
	public:
		/********************************************************************
		* create new professional according the string of the type, and 	*
		* 	return it														*
		*********************************************************************/
		Professional* create(typeProf type, unsigned int id, string name,
				string description, int age, string gen);

};


#endif /* PROFESSIONALFACTORY_H_ */

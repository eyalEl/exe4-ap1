/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Screen-writer, type *
* of professional						*
****************************************/

#include"ScreenWriter.h"
#include<iostream>

/********************************************************************
 * create new screenwriter											*
 ********************************************************************/
ScreenWriter::ScreenWriter(unsigned int id, string name, string description, int age, string gen) :
Professional(id, name, description, age, gen){}

/********************************************************************
 * print the screen-writer, his name and job description			*
 ********************************************************************/
void ScreenWriter::print(InputOutput*io){
	io->print(this->getName()+ " "+ this->getJobDescrip());
	io->print("\n");
}

/********************************************************************
* delete the screenWriter											*
*********************************************************************/
ScreenWriter::~ScreenWriter(){}

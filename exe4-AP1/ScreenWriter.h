/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: Screen-writer, type *
* of professional						*
****************************************/

#ifndef SCREENWRITER_H_
#define SCREENWRITER_H_
#include"Professional.h"
#include<string>
using namespace std;
/************************************************************
* the class of the screenwriter								*
*************************************************************/
class ScreenWriter: public Professional
{
	public:
		/********************************************************************
		 * create new screenwriter											*
		 ********************************************************************/
		ScreenWriter(unsigned int id, string name, string description, int age, string gen);
		/********************************************************************
		* print the information that producer need to print when the user	*
		* 	 ask for														*
		*********************************************************************/
		void print(InputOutput*io);
		/********************************************************************
		* delete the screenWriter											*
		*********************************************************************/
		~ScreenWriter();
};



#endif /* SCREENWRITER_H_ */

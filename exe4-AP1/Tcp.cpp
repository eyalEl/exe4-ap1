/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the TCP protocol	*
****************************************/

#include "Tcp.h"
/****************************************************************
 * send the number of the socket to the father class			*
 ****************************************************************/
Tcp::Tcp(int scoketNumber):CommunicationProtocol(scoketNumber){}

/****************************************************************
* send the get buffer to the socket								*
*****************************************************************/
int Tcp::sendToSokect(char*buffer, int sizeMsg){
    int sentBytes = send(this->socketNum, buffer, sizeMsg, 0);
    if (sentBytes < 0) {
        perror("error sending to client");
    }
    return sentBytes;
}

/****************************************************************
* get from the socket and enter to the buffer					*
*****************************************************************/
int Tcp::receiveFromSocket(char*buffer, int sizeBuf){
	int readBytes = recv(this->socketNum, buffer, sizeBuf, 0);
	return readBytes;
}

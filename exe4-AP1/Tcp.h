/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the TCP protocol	*
****************************************/

#ifndef TCP_H_
#define TCP_H_

#include "CommunicationProtocol.h"

class Tcp: public CommunicationProtocol {
	public:
		/****************************************************************
		* create the protocol, and save the socket						*
		*****************************************************************/
		Tcp(int scoketNumber);
	protected:
		/****************************************************************
		* send the get buffer to the socket								*
		*****************************************************************/
		int sendToSokect(char*buffer, int sizeMsg);
		/****************************************************************
		* get from the socket and enter to the buffer					*
		*****************************************************************/
		int receiveFromSocket(char*buffer, int sizeBuf);
};

#endif /* TCP_H_ */

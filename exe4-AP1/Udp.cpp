/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the UDP protocol	*
****************************************/

#include "Udp.h"

/****************************************************************
* create the TCP protocol										*
*****************************************************************/
Udp::Udp(int port):CommunicationProtocol(port){}
/****************************************************************
* send the get buffer to the socket								*
*****************************************************************/
int Udp::sendToSokect(char*buffer, int sizeMsg){
	struct sockaddr_in sin;
    int sentBytes = sendto(this->socketNum, buffer, sizeMsg, 0,
    		(struct sockaddr *) &sin, sizeof(sin));
    if (sentBytes < 0) {
        perror("error writing to socket");
    }
    return sentBytes;
}
/****************************************************************
* get from the socket and enter to the buffer					*
*****************************************************************/
int Udp::receiveFromSocket(char*buffer, int sizeBuf){
	struct sockaddr_in sin;
    memset(buffer, 0, sizeBuf);
	unsigned int from_len = sizeof(struct sockaddr_in);
    int readByte = recvfrom(this->socketNum, buffer, sizeBuf,
    		0, (struct sockaddr *) &sin, &from_len);
    if (readByte < 0) {
        perror("error reading from socket");
    }
    return readByte;
}

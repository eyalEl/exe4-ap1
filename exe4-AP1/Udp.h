/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: the UDP protocol	*
****************************************/

#ifndef UDP_H_
#define UDP_H_

#include "CommunicationProtocol.h"

class Udp: public CommunicationProtocol {
	public:
		/****************************************************************
	 	* create the TCP protocol										*
		*****************************************************************/
		Udp(int port);
	protected:
		/****************************************************************
		* send the get buffer to the socket								*
		*****************************************************************/
		int sendToSokect(char*buffer, int sizeMsg);
		/****************************************************************
		* get from the socket and enter to the buffer					*
		*****************************************************************/
		int receiveFromSocket(char*buffer, int sizeBuf);
};

#endif /* UDP_H_ */

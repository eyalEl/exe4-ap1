/* 8921003 307839803 Racheli Gruffi*/
/* 8921004 315711804 Eyal Elboim*/

/****************************************
* Student Name: Racheli Gruffi, Eyal Elboim             *
* Exercise Name: exe5               	*
* File description: start the server and*
* 	the menu
****************************************/

#include"Menu.h"
#include "CreatThread.h"
#define PROTOCOL_TYPE 1
#define PORT_NUM 2

/*********************************************************************
* Program name: exe5- sever										     *
* The operation: the server of the program 							 *
*********************************************************************/
//in the argv will be the port number and the type of the protocol
int main(int argc, char*argv[]){
	CreatThread thread(argv[PROTOCOL_TYPE],argv[PORT_NUM]);
	thread.start();
	return 0;
}

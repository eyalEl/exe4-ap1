/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: run the client 			*
********************************************/
#define TCP 1
#define UDP 0
#define STOP "-1"
#include "Client.h"
#include <unistd.h>
#include <string.h>
#include <iostream>
#include "Input.h"
#include "Output.h"
#include "Tcp.h"
#include "Udp.h"
using namespace std;

/***************************************************
 * Constructor									   *
 **************************************************/
Client::Client(int kind,char* ip,int port) {
	if(kind==TCP){
		this->comunication=new Tcp(port,ip);//the way to communicate will be Tcp
	}else{
		this->comunication=new Udp(port,ip);//the way to communicate will be Udp
	}
}

/***************************************************
 * Distructor										*
 **************************************************/
Client::~Client() {
	delete this->comunication;
}

/********************************************************
 *Name:start											*
 *Description: this function run all the communication	*
 *with the user and the server.transfer the information	*
 *between those two										*
 *******************************************************/
void Client::start(){
	string input;
	Input in;
	Output out;
	input=in.getLine();//get the input from user
	char buf[4096];
	while(input.compare(STOP)!=0){
		strcpy(buf,input.c_str());//convert the string "input" to char*
		this->comunication->sendToSokect(buf);//send to server the input of the user
		input=this->comunication->receiveFromSocket();//get the information to print from the server
		out.print(input);//print the information
		input=in.getLine();//get the new line from user
	}
	strcpy(buf,STOP);
	this->comunication->sendToSokect(buf);//send to server that the user decided to stop communication
}

/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: run the client 			*
********************************************/

#ifndef CLIENT_H_
#define CLIENT_H_

#include "CommunicationProtocol.h"
class Client {
private:
	CommunicationProtocol* comunication;
public:
	/***************************************************
	 * Constructor									   *
	 **************************************************/
	Client(int kind,char* ip,int port);

	/***************************************************
	 * Distructor										*
	 **************************************************/
	virtual ~Client();

/********************************************************
 *Name:start											*
 *Description: this function run all the communication	*
 *with the user and the server.transfer the information	*
 *between those two										*
 *******************************************************/
	void start();
};

#endif /* CLIENT_H_ */

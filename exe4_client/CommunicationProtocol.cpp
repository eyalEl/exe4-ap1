/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the abstract			*
* protocol, hold the share code				*
********************************************/
#include "CommunicationProtocol.h"

/***************************************************
 * Constructor									   *
 **************************************************/
CommunicationProtocol::CommunicationProtocol(int port,char* ip){
	this->portNumber = port;
	this->ip=ip;
	this->theSocket = SOCKET_DEFAULT;
}

/***************************************************
 * Distructor									   *
 **************************************************/
CommunicationProtocol::~CommunicationProtocol(){
	close(this->theSocket);
}
/********************************************************
 *Name:startSocket										*
 *Description: this function run the things that UDP
 *and TCP do the same
 *******************************************************/
void CommunicationProtocol::startSocket(){

	memset(&sin, 0, sizeof(this->sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr(this->ip);
	sin.sin_port = htons(this->portNumber);

}

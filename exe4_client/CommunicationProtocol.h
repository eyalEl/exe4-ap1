/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the abstract			*
* protocol, hold the share code				*
********************************************/

#ifndef COMMUNICATIONPROTOCOL_H_
#define COMMUNICATIONPROTOCOL_H_
#include <iostream>
#include <sys/socket.h>
#include <string>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
using namespace std;
#define SOCKET_DEFAULT -1;

class CommunicationProtocol {
	private:
		int portNumber;
	protected:
	    struct sockaddr_in sin;
		int theSocket;
		char* ip;
		void startSocket();
		/***************************************************
		 * Constructor									   *
		 **************************************************/
		CommunicationProtocol(int port,char* ip);
	public:
		/***************************************************
		 * Distructor										*
		 **************************************************/
		virtual ~CommunicationProtocol();

		/**************************************************
		 *Name:sendToSokect
		 *Description:send buffer to server throw the
		 *correct socket
		 *************************************************/
		virtual void sendToSokect(char*buffer)= 0;

		/**************************************************
		 *Name:receiveFromSocket
		 *Description:receive to buffer from server throw
		 *the correct socket
		 *************************************************/
		virtual string receiveFromSocket()= 0;
};

#endif /* COMMUNICATIONPROTOCOL_H_ */

/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description:has the responsibility to*
* get the input from user					*
********************************************/

#include "Input.h"

/***************************************************
 * Constructor									   *
 **************************************************/
Input::Input() {
	// TODO Auto-generated constructor stub

}

/***************************************************
 * Distructor										*
 **************************************************/
Input::~Input() {
	// TODO Auto-generated destructor stub
}
/************************************************************
 *Name:getLine												*
 *Description:get the line from user and send it to client	*
 ************************************************************/
string Input::getLine(){
	string input;
	getline(cin,input);
	return input;
}


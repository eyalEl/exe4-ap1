/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description:has the responsibility to*
* get the input from user					*
********************************************/

#ifndef INPUT_H_
#define INPUT_H_
#include <unistd.h>
#include <string.h>
#include <iostream>
using namespace std;
class Input {
public:
	/***************************************************
	 * Constructor									   *
	 **************************************************/
	Input();

	/***************************************************
	 * Distructor									   *
	 **************************************************/
	virtual ~Input();

	/***************************************************
	 * Name:getLine									   *
	 * Description:this function get a line from user  *
	 * and return it								   *
	 **************************************************/
	string getLine();
};

#endif /* INPUT_H_ */

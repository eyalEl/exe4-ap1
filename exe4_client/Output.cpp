/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description:has the responsibility to*
* print the input to user					*
********************************************/
#include "Output.h"

/***************************************************
 * Constructor									   *
 **************************************************/
Output::Output() {
	// TODO Auto-generated constructor stub

}

/***************************************************
 * Distructor										*
 **************************************************/
Output::~Output() {
	// TODO Auto-generated destructor stub
}

/****************************************************
 * Name:print										*
 * Description: get a string and print it to user	*
 ***************************************************/
void Output::print(string std){
	cout<<std;
	return;
}

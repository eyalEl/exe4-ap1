/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description:has the responsibility to*
* print the input to user					*
********************************************/

#ifndef OUTPUT_H_
#define OUTPUT_H_
#include <unistd.h>
#include <string.h>
#include <iostream>
using namespace std;

class Output {
public:
	/***************************************************
	 * Constructor									   *
	 **************************************************/
	Output();

	/****************************************************
	 * Distructor										*
	 ***************************************************/
	virtual ~Output();

	/****************************************************
	 * Name:print										*
	 * Description: get a string and print it to user	*
	 ***************************************************/
	void print(string std);
};

#endif /* OUTPUT_H_ */

/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the TCP protocol		*
********************************************/
#include "Tcp.h"

/***************************************************
 * Constructor									   *
 **************************************************/
Tcp::Tcp(int port,char* ip):CommunicationProtocol(port,ip){
	this->theSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (this->theSocket < 0) {
		perror("error creating socket");
	}
	this->startSocket();

	if (connect(this->theSocket, (struct sockaddr *) &this->sin, sizeof(this->sin)) < 0) {
		perror("error connecting to server");
	}
}

/**************************************************
 *Name:sendToSokect
 *Description:send buffer to server throw the
 *correct socket
 *************************************************/
void Tcp::sendToSokect(char*buffer){
    int sent_bytes = send(this->theSocket, buffer, strlen(buffer), 0);
    if (sent_bytes < 0) {
    	perror("error sending");
    }
}
/**************************************************
 *Name:receiveFromSocket
 *Description:receive to buffer from server throw
 *the correct socket
 *************************************************/
string Tcp::receiveFromSocket(){
	string toReturn;
	char buffer[4096]={0};// need to reset
	int expected_data_len = sizeof(buffer);
	int read_bytes = recv(this->theSocket, buffer, expected_data_len, 0);
	if (read_bytes < 0) {
		perror("error receive");
	}
	else {
		toReturn=string(buffer);
		return toReturn;
	}
	return toReturn;
}

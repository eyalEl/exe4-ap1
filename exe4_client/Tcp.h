/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the TCP protocol		*
********************************************/
#ifndef TCP_H_
#define TCP_H_

#include "CommunicationProtocol.h"
#include <iostream>
#include <cstdlib>
using namespace std;

class Tcp: public CommunicationProtocol {
	public:
		/***************************************************
		 * Constructor									   *
		 **************************************************/
		Tcp(int port,char* ip);
	protected:
		/**************************************************
		 *Name:sendToSokect
		 *Description:send buffer to server throw the
		 *correct socket
		 *************************************************/
		void sendToSokect(char*buffer);
		/**************************************************
		 *Name:receiveFromSocket
		 *Description:receive to buffer from server throw
		 *the correct socket
		 *************************************************/
		string receiveFromSocket();
};

#endif /* TCP_H_ */

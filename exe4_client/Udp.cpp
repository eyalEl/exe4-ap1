/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the UDP protocol		*
********************************************/
#include "Udp.h"

/***************************************************
 * Constructor									   *
 **************************************************/
Udp::Udp(int port,char* ip):CommunicationProtocol(port,ip){

	this->theSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (theSocket < 0) {
		perror("error creating socket");
	}
	this->startSocket();
}

/**************************************************
 *Name:sendToSokect
 *Description:send buffer to server throw the
 *correct socket
 *************************************************/
void Udp::sendToSokect(char*buffer){
	int sent_bytes = sendto(this->theSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &this->sin, sizeof(this->sin));
	if (sent_bytes < 0) {
		perror("error writing to socket");
	}
return;
}

/**************************************************
 *Name:receiveFromSocket
 *Description:receive to buffer from server throw
 *the correct socket
 *************************************************/
string Udp::receiveFromSocket(){
	string toReturn;
	struct sockaddr_in from;
	unsigned int from_len = sizeof(struct sockaddr_in);
	char buffer[4096]={0};// need to reset buffer
	memset(&buffer, 0, sizeof(buffer));
	int bytes = recvfrom(this->theSocket, buffer, sizeof(buffer), 0, (struct sockaddr *) &from, &from_len);
	if (bytes < 0) {
		perror("error reading from socket");
	}
	toReturn=string(buffer);
	return toReturn;
}

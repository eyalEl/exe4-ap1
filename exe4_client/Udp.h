/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description: the UDP protocol		*
********************************************/
#ifndef UDP_H_
#define UDP_H_
#include <iostream>
#include <cstdlib>
#include "CommunicationProtocol.h"
using namespace std;

class Udp: public CommunicationProtocol {
	public:
		/***************************************************
		 * Constructor									   *
		 **************************************************/
		Udp(int port,char* ip);
	protected:
		/**************************************************
		 *Name:sendToSokect
		 *Description:send buffer to server throw the
		 *correct socket
		 *************************************************/
		void sendToSokect(char*buffer);

		/**************************************************
		 *Name:receiveFromSocket
		 *Description:receive to buffer from server throw
		 *the correct socket
		 *************************************************/
		string receiveFromSocket();
};

#endif /* UDP_H_ */

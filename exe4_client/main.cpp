/* 8921003 307839803 Racheli Gruffi*/
/* 8921004 315711804 Eyal Elboim*/
/********************************************
* Student Name: Racheli Gruffi, Eyal Elboim *
* Exercise Name: exe5               		*
* File description:run the client			*
********************************************/
#include <iostream>
#include <cstdlib>
#include "Client.h"
using namespace std;
/*********************************************************************
* Program name: exe5- client										     *
* The operation: the client of the program 							 *
*********************************************************************/

int main(int argc,char* argv[]){
	int kind=atoi(argv[1]);
	char* ip=argv[2];
	int port =atoi(argv[3]);
	Client client(kind,ip,port);
	client.start();
}


